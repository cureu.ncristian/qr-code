import { useState, useRef } from "react";
import { Html5Qrcode } from "html5-qrcode";

function useQRCodeScan({
  qrcodeMountNodeID = "",
  closeAfterScan = true,
  getQrBoxDimension,
}) {
  const [decodedQRData, setDecodedQrData] = useState({
    isScanning: false,
    isScanSuccess: false,
    isScanFailure: false,
    data: null,
    error: "",
  });
  const html5QrCodeScannerRef = useRef(null);

  const startQrCode = () => {
    try {
      setDecodedQrData({
        ...decodedQRData,
        isScanning: true,
        data: null,
      });
      const html5qrCodeScanner = new Html5Qrcode(qrcodeMountNodeID);
      html5QrCodeScannerRef.current = html5qrCodeScanner;
      let qrbox = 250;
      if (getQrBoxDimension) {
        qrbox = getQrBoxDimension();
      }
      html5qrCodeScanner
        .start(
          { facingMode: "environment" },
          { fps: 100, qrbox, aspectRatio: 1.777778 },
          (qrCodeMessage) => {
            //Daca codul qr este scanat cu succes, mesajul vine in parametrul qrCodeMessage
            console.log("scanned qr code", qrCodeMessage);
            setDecodedQrData({
              ...decodedQRData,
              isScanSuccess: true,
              isScanning: false,
              data: qrCodeMessage,
              error: "",
            });
            //Daca closeAfterScan este setat false, nu se opreste camera dupa ce scaneaza codul cu succes
            //Daca este true, se opreste camera in cazul in care promise-ul returnat de stop()
            //este rezolvat cu succes
            if (closeAfterScan) {
              html5qrCodeScanner
                .stop()
                .then(() => {
                  // QR Code scanning is stopped.
                  console.log("stopped after successful scan");
                })
                .catch((err) => {
                  console.log(
                    "fails to stop after succesfull scan result ",
                    err
                  );
                });
            }
          }
        ) //Daca promise-ul returnat de start() nu este rezolvat cu succes, se intra pe catch
        .catch((err) => {
          setDecodedQrData({
            ...decodedQRData,
            isScanSuccess: false,
            isScanning: false,
            isScanFailure: true,
            data: null,
            error: err || "QR Code parsing failed",
          });
        });
    } catch (err) {
      //Daca nu se creaza cu succes o instanta a clasei Html5Qrcode, se intra pe catch
      setDecodedQrData({
        ...decodedQRData,
        isScanSuccess: false,
        isScanning: false,
        isScanFailure: true,
        data: null,
        error: err || "QR Code parsing failed",
      });
    }
  };

  const stopQrCode = () => {
    if (html5QrCodeScannerRef.current) {
      html5QrCodeScannerRef.current
        .stop()
        .then(() => {
          // QR Code scanning is stopped
          console.log("camera stopped");
          setDecodedQrData({
            ...decodedQRData,
            isScanning: false,
          });
        })
        .catch((err) => {
          // Stop failed
          console.log("fails to stop after successfull scan result", err);
        });
    }
  };

  return {
    startQrCode,
    decodedQRData,
    stopQrCode,
  };
}

export default useQRCodeScan;
