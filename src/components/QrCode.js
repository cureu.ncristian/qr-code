import "./qr.css";
import { useEffect } from "react";
import useQRCodeScan from "../hooks/useQRCodeScan";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import QrCodeScannerOutlinedIcon from "@mui/icons-material/QrCodeScannerOutlined";
import StopCircleOutlinedIcon from "@mui/icons-material/StopCircleOutlined";

function QrCode() {
  const { startQrCode, decodedQRData, stopQrCode } = useQRCodeScan({
    qrcodeMountNodeID: "qrcodemountnode",
  });

  useEffect(() => {
    console.log(decodedQRData);
  }, [decodedQRData]);

  return (
    <div className="qr">
      <div className="cameraContainer">
        <div id="qrcodemountnode" className="camera"></div>
        {decodedQRData.data !== null && (
          <div className="result">{decodedQRData.data}</div>
        )}
      </div>
      <div className="buttons">
        <Button
          onClick={() => startQrCode()}
          variant="outlined"
          endIcon={<QrCodeScannerOutlinedIcon />}
          sx={{ transform: "scale(1.3)", letterSpacing: "1px" }}
        >
          Scan
        </Button>
        {decodedQRData.isScanning && (
          <IconButton onClick={() => stopQrCode()} color="error">
            <StopCircleOutlinedIcon />
          </IconButton>
        )}
      </div>
    </div>
  );
}

export default QrCode;
